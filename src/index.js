import {useEffect, useState} from 'react';
import {View, Dimensions, StyleSheet} from 'react-native';

const {width, height} = Dimensions.get('window');

const useStyles = (appWidth, appHeight, appOrientation) => {
  // THEN ANG WIDTH UG HEIGHT NGA IMO GI PASS KAY GAMITON NIMO
  // SA MGA COMPONENTS SIZE
  return StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      width: appWidth,
    },
    imageHeight: {
      width: appWidth * 0.7, //40%
      height: appOrientation === 'PORTRAIT' ? appHeight * 0.2 : appHeight * 0.7, //15%
      backgroundColor: 'red',
    },
  });
};

const App = () => {
  // THEN PWEDE KA MO GAMIT SA ORIENTATION STATE TO RENDER SPECIFIC STYLE FOR PORTRAIT AND LANDSCAPE
  // i.e if PORTRAIT render this | if LANDSCAPE render this
  const [orientation, setOrientation] = useState('PORTRAIT');

  const [currentScreenSize, setCurrentScreenSize] = useState({width, height});
  // OR PWEDE NIMO E PASS ANG CURRENT WIDTH AND HEIGHT SA SCREEN SA STYLE
  const Styles = useStyles(
    currentScreenSize.width,
    currentScreenSize.height,
    orientation,
  );

  useEffect(() => {
    // SET THE LISTENER TO GET THE CURRENT WIDTH AND HEIGHT OF THE SCREEN
    Dimensions.addEventListener('change', e => {
      const {width, height} = e.window;

      setCurrentScreenSize({width, height});
      setOrientation(width > height ? 'LANDSCAPE' : 'PORTRAIT');
    });
  }, []);

  console.log('orientation', orientation);
  console.log('currentScreenSize', currentScreenSize);

  return (
    <View style={Styles.container}>
      <View style={Styles.imageHeight}></View>
    </View>
  );
};

export default App;
